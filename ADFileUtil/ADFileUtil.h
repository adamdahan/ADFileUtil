#import <Foundation/Foundation.h>

@interface ADFileUtil : NSObject

+ (NSString *)documentDirectoryPath;
+ (NSString *)libraryDirectoryPath;
+ (NSString *)libraryCachesDirectoryPath;

+ (NSString *)directoryPath:(NSSearchPathDirectory)searchPath;
+ (NSString *)pathForDirectoryInDocumentsDirectory:(NSString *)directoryName;
+ (NSString *)pathForDirectory:(NSString *)directory
                withSearchPath:(NSSearchPathDirectory)searchPath;

+ (BOOL)createDirectoryInDocumentsDirectory:(NSString *)directoryName;
+ (BOOL)createDirectoryInLibraryDirectory:(NSString *)directoryName;
+ (BOOL)createDirectoryInLibraryCachesDirectory:(NSString *)directoryName;

+ (void)removeDirectory:(NSString *)directory;
+ (void)removeDirectory:(NSString *)directory
         fromSearchPath:(NSSearchPathDirectory)searchPath;

+ (void)saveImage:(UIImage *)image
         withName:(NSString *)name
     toSearchPath:(NSSearchPathDirectory)searchPath
       completion:(void(^)(NSString *path))completionBlock;

+ (void)saveImage:(UIImage *)image
         withName:(NSString *)name
      toDirectory:(NSString *)directory
       completion:(void(^)(NSString *path))completionBlock;

+ (UIImage *)loadImageNamed:(NSString *)imageName
             fromSearchPath:(NSSearchPathDirectory)searchPath;

+ (UIImage *)loadImageNamed:(NSString *)imageName
              fromDirectory:(NSString *)directory;

@end
