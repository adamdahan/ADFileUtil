#import "ADFileUtil.h"

@implementation ADFileUtil

+ (NSString *)documentDirectoryPath
{
	return [self directoryPath:NSDocumentDirectory];
}

+ (NSString *)libraryDirectoryPath
{
	return [self directoryPath:NSLibraryDirectory];
}

+ (NSString *)libraryCachesDirectoryPath
{
    return [self directoryPath:NSCachesDirectory];
}

+ (NSString *)directoryPath:(NSSearchPathDirectory)searchPath
{
    return [NSSearchPathForDirectoriesInDomains(searchPath, NSUserDomainMask, YES) lastObject];
}

+ (BOOL)createDirectoryInLibraryCachesDirectory:(NSString *)directoryName
{
    @autoreleasepool
    {
        BOOL success = NO;
        // DO NOT use defaultManager as that is NOT thread safe
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSString *dir = [[self libraryCachesDirectoryPath] stringByAppendingPathComponent:directoryName];
        
        NSError *error;
        if (![fileManager fileExistsAtPath:dir])
        {
            success = [fileManager createDirectoryAtPath:dir withIntermediateDirectories:NO attributes:nil error:&error];
        }
        return success;
    }
}

+ (BOOL)createDirectoryInDocumentsDirectory:(NSString *)directoryName
{
    @autoreleasepool
    {
        BOOL success = NO;
        // DO NOT use defaultManager as that is NOT thread safe
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSString *dir = [[self documentDirectoryPath] stringByAppendingPathComponent:directoryName];
        
        NSError *error;
        if (![fileManager fileExistsAtPath:dir])
        {
            success = [fileManager createDirectoryAtPath:dir withIntermediateDirectories:NO attributes:nil error:&error];
        }
        return success;
    }
}

+ (BOOL)createDirectoryInLibraryDirectory:(NSString *)directoryName
{
    @autoreleasepool
    {
        BOOL success = NO;
        // DO NOT use defaultManager as that is NOT thread safe
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSString *dir = [[self libraryDirectoryPath] stringByAppendingPathComponent:directoryName];
        
        NSError *error;
        if (![fileManager fileExistsAtPath:dir])
        {
            success = [fileManager createDirectoryAtPath:dir withIntermediateDirectories:NO attributes:nil error:&error];
        }
        return success;
    }
}

+ (NSString *)pathForDirectoryInDocumentsDirectory:(NSString *)directoryName
{
    return [[self documentDirectoryPath] stringByAppendingPathComponent:directoryName];
}

+ (NSString *)pathForDirectory:(NSString *)directory withSearchPath:(NSSearchPathDirectory)searchPath
{
    return [[self directoryPath:searchPath] stringByAppendingPathComponent:directory];
}

+ (void)removeDirectory:(NSString *)directory fromSearchPath:(NSSearchPathDirectory)searchPath
{
    // DO NOT USE defaultManager as it is NOT thread safe
    NSFileManager *manager = [[NSFileManager alloc] init];
    NSError *error;
    if ([manager fileExistsAtPath:[self pathForDirectory:directory withSearchPath:searchPath]])
        [manager removeItemAtPath:[self pathForDirectory:directory withSearchPath:searchPath]
                            error:&error];
    if (error)
        NSLog(@"%@", error);
}

+ (void)removeDirectory:(NSString *)directory
{
    // DO NOT USE defaultManager as it is NOT thread safe
    NSFileManager *manager = [[NSFileManager alloc] init];
    NSError *error;
    if ([manager fileExistsAtPath:[self pathForDirectoryInDocumentsDirectory:directory]])
        [manager removeItemAtPath:[self pathForDirectoryInDocumentsDirectory:directory]
                            error:&error];
    if (error)
        NSLog(@"%@", error);
}

+ (void)saveImage:(UIImage *)image
         withName:(NSString *)name
     toSearchPath:(NSSearchPathDirectory)searchPath
       completion:(void(^)(NSString *path))completionBlock;
{
    if (image)
    {
        NSString *path = [[self directoryPath:searchPath] stringByAppendingPathComponent:name];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path
               atomically:YES];
        
        if (completionBlock)
            completionBlock(path);
    }
}

+ (void)saveImage:(UIImage *)image
         withName:(NSString *)name
      toDirectory:(NSString *)directory
       completion:(void(^)(NSString *path))completionBlock;
{
    if (image)
    {
        NSString *path = [[self pathForDirectoryInDocumentsDirectory:directory] stringByAppendingPathComponent:name];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path
               atomically:YES];
        
        if (completionBlock)
            completionBlock(path);
    }
}

+ (UIImage *)loadImageNamed:(NSString *)imageName
             fromSearchPath:(NSSearchPathDirectory)searchPath
{
    return [UIImage imageWithContentsOfFile:[[ADFileUtil directoryPath:searchPath] stringByAppendingPathComponent:imageName]];
}

+ (UIImage *)loadImageNamed:(NSString *)imageName
              fromDirectory:(NSString *)directory
{
    return [UIImage imageWithContentsOfFile:[[ADFileUtil pathForDirectoryInDocumentsDirectory:directory] stringByAppendingPathComponent:imageName]];
}

@end
