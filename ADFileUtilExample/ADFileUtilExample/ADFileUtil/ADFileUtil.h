#import <UIKit/UIKit.h>

@interface ADFileUtil : NSObject

+ (NSString *)documentDirectoryPath;
+ (NSString *)libraryDirectoryPath;
+ (NSString *)libraryCachesDirectoryPath;

+ (NSString *)directoryPath:(NSSearchPathDirectory)searchPath;
+ (NSString *)pathForDirectory:(NSString *)directory;
+ (NSString *)pathForDirectory:(NSString *)directory
                withSearchPath:(NSSearchPathDirectory)searchPath;

+ (BOOL)createDirectoryInDocumentsDirectory:(NSString *)directoryName;
+ (BOOL)createDirectoryInLibraryDirectory:(NSString *)directoryName;
+ (BOOL)createDirectoryInLibraryCachesDirectory:(NSString *)directoryName;

+ (void)removeDirectory:(NSString *)directory;
+ (void)removeDirectory:(NSString *)directory
         fromSearchPath:(NSSearchPathDirectory)searchPath;

- (UIImage *)loadImageNamed:(NSString *)imageName
             fromSearchPath:(NSSearchPathDirectory)searchPath;

- (UIImage *)loadImageNamed:(NSString *)imageName
              fromDirectory:(NSString *)directory;

@end
