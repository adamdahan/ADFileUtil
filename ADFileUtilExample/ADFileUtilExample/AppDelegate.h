//
//  AppDelegate.h
//  ADFileUtilExample
//
//  Created by Adam Dahan on 1/18/2014.
//  Copyright (c) 2014 Adam Dahan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
