//
//  ViewController.m
//  ADFileUtilExample
//
//  Created by Adam Dahan on 1/18/2014.
//  Copyright (c) 2014 Adam Dahan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
