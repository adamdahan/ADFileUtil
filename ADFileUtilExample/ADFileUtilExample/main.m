//
//  main.m
//  ADFileUtilExample
//
//  Created by Adam Dahan on 1/18/2014.
//  Copyright (c) 2014 Adam Dahan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
