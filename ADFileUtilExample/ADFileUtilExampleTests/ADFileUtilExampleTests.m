//
//  ADFileUtilExampleTests.m
//  ADFileUtilExampleTests
//
//  Created by Adam Dahan on 1/18/2014.
//  Copyright (c) 2014 Adam Dahan. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface ADFileUtilExampleTests : XCTestCase

@end

@implementation ADFileUtilExampleTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
